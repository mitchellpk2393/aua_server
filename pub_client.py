import socket
import time
import uuid
from Crypto.Cipher import AES
import hashlib

# UUID = uuid.uuid3(uuid.NAMESPACE_DNS, 'DEVICE 1')

PASSCODE = "DERWIN".encode()
KEY = hashlib.sha256(PASSCODE).digest()
mode = AES.MODE_CBC
IV = 'this is an IV456'.encode()  # must be 16 char long and normally changed every time maybe using cryptographer fernet

cipher = AES.new(KEY, mode, IV)

CLIENT_ID = "16fd2706-8baf-433b-82eb-8c7fada847da#"
CLIENT_ID2 = "1234#"

location = ["$GPRMC,181544.000,A,5153.8558,N,00429.7914,E,1.6,28.8,080720,,,A*5D",
            "$GPRMC,181545.000,A,5153.8566,N,00429.7925,E,0.3,28.6,080720,,,A*59",
            "$GPRMC,181546.000,A,5153.8561,N,00429.7922,E,1.1,20.4,080720,,,A*53",
            "$GPRMC,181546.000,A,5153.8561,N,00429.7922,E,1.1,20.4,080720,,,A*53",
            "$GPRMC,181547.000,A,5153.8561,N,00429.7916,E,1.5,3.0,080720,,,A*64",
            "$GPRMC,181548.000,A,5153.8558,N,00429.7911,E,2.1,316.5,080720,,,A*63",
            "$GPRMC,181611.000,A,5153.8597,N,00429.7740,E,1.7,149.4,080720,,,A*69",
            "$GPRMC,181613.000,A,5153.8593,N,00429.7746,E,0.2,145.7,080720,,,A*62",
            ]

data = {'vec': IV,
        'gps': {'ID': CLIENT_ID, 'NMEA': location[7]}}

HEADER = 64
PORT = 5050  # above 4000 maybe no in use
FORMAT = "utf-8"
END_MSG = "END"

SERVER = socket.gethostbyname(socket.gethostname())  # flexible
ADDR = (SERVER, PORT)
#client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#client.connect(ADDR)


def make_new_conn(server=None, addr=None, client=None, port=PORT):
    server = socket.gethostbyname(socket.gethostname())
    addr = (server, port)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(addr)
    return client


def pad_msg(msg):
    while len(msg) % 16 != 0:
        msg = msg + " "
    return msg


def send(msg):
    data = msg.encode(FORMAT)  # sending byte over to socket
    msg_length = len(data)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))  # fill msg to desire header size
    client.send(send_length)
    client.send(data)
    # client.close()


def crypt_send(vec, gps):
    # TODO send crypt msg to server and decode at the server, first has to be send
    # TODO try to send everything in one payload, vec should be append first without encrypt then follow gps data
    padded = pad_msg(gps['ID'] + gps['NMEA'])
    gps = cipher.encrypt(padded.encode(FORMAT))
    msg = vec + gps  # accumulated the payload
    msg_length = len(msg)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))  # fill msg to desire header size
    client.send(send_length)
    client.send(msg)


while True:
    client = make_new_conn()
    cipher = AES.new(KEY, mode, IV)
    crypt_send(**data)
    client.send(bytes('128', 'utf-8'))
    client.send(bytes('abc', 'utf-8'))
    #crypt_send(**data)
    send(END_MSG)

    time.sleep(10)

# crypt_send(END_MSG)
# time.sleep(2)
# crypt_send(CLIENT_ID + location[0])
# time.sleep(2)
# crypt_send(CLIENT_ID + location[0])
# time.sleep(2)
# crypt_send(END_MSG)
# crypt_send(END_MSG)
