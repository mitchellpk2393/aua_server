import socket
import threading
from termcolor import colored
import json
import pynmea2
import os
import time
import uuid
# from Crypto.Cipher import AES

import hashlib
import aes
import sys

# TODO GSM MODULE first attempted
# TODO make error expection when receving strings as msg length and not numbers

path = './'
filename = 'database'

ALL_NODE_ID = {
    "GPS": "1234",
    "GPS_1": "16fd2706-8baf-433b-82eb-8c7fada847da",
    "GPS_2": "5678",
    "GPS_3": "910112",
    "GPS_4": "13141516",
}
##############################
PASSCODE = "DERWIN".encode()
KEY = hashlib.sha256(PASSCODE).digest()
# mode = AES.MODE_CBC
# must be 16 bytes long and normally changed every time maybe using cryptographer fernet
IV = 'this is an IV456'.encode()
cipher = 0

####################################

HEADER = 128
PORT = 5050  # above 4000 maybe no in use
# SERVER = '192.167.178.19' # hard coded connection over di internet must be changed to public iP
SERVER = socket.gethostbyname(socket.gethostname())  # flexible
FORMAT = "utf-8"
END_MSG = "END"
HEAD_MSG = "HEAD#"
TAIL_MSG = "#TAIL"

# what type of adress are we looking for ipv4 ,streaming TCP data on socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# binding it to adress
ADDR = (SERVER, PORT)
# all connection wil be past to this socket
try:
    server.bind(ADDR)
    # server.settimeout(5)
except Exception as e:
    raise SystemExit(f"could not bind host {SERVER}, to port:{PORT}, because: {e}")

# create_database
file = open("database.csv", 'w')
file.writelines("ID,Time,Valid,lat,N/S,long,E/W,speed_ground,course_speed,date\n")


def get_key(val):  # function to return key for any value
    for key, value in ALL_NODE_ID.items():
        if val == value:
            return key

    return "key doesn't exist"


def write_to_csv(data, node):
    cloud = {"id": "not added yet",
             "time": data[0],
             "valid": data[1],
             "lat": data[2],
             "north_south": data[3],
             "long": data[4],
             "east_west": data[5],
             "speed_ground": data[6],
             "course_speed": data[7],
             "date": data[8]}
    # timestamp = float(time.clock())
    file.writelines(
        node + "," + data[0] + "," + data[1] + "," + data[2] + "," + data[3] + "," + data[4] + "," + data[5] + "," +
        data[6] + "," + data[7] + "," + data[8] + "\n")
    file.flush()  # insert writeline in file


sim = 0


def incoming_is_valid(buff):
    global sim
    text = bytearray(buff)
    head = 0
    tail = 0
    # get summation of ascii from the head,tail
    try:
        for i in range(0, 4):
            head += text[i]
        for x in range(1, 4):
            tail += text[-x]
    except IndexError:
        print("the sim module is bugged didnt receive correct payload")
        sim += 1
        pass
    # remove tcp head and tail
    if head == 274 and tail == 214:
        del text[0:5]
        for x in range(1, 6):
            if x == 1:
                del text[-x]
            else:
                del text[len(text) - 1]
        return text
    else:
        return None


def handle_client(conn, addr, old_iv_key=None):
    global decrypted, msg_length, sim
    print(f"NEW CONN: {addr} connected \n")
    connected: bool = True
    deadline = time.time() + 4
    while connected:
        # TODO try recv  expt ConnectionResetError: (module lost conn or shutdown)
        the_sim = conn.recv(HEADER)  # how many bytes where expecting,
        # ConnectionResetError when recv
        # print(the_sim)
        if not the_sim:
            if time.time() >= deadline:
                connected = False
                conn.close()
                print(f"DROP CONN for : {addr} \n")
                sim = sim + 1
                print("missed counter={}".format(sim))
                break

        try:
            try:
                msg_length = int((the_sim.decode(FORMAT)))  # get msg length format base10 bytes
            except ValueError:
                continue
            if msg_length == 13:
                the_sim = incoming_is_valid(conn.recv(msg_length).decode(FORMAT))
                if the_sim == END_MSG:
                    connected = False
            elif msg_length > 13:
                msg = bytearray(conn.recv(msg_length))  # receive raw
                msg = incoming_is_valid(msg)
                if msg is None: break
                new_key = (msg[0:16])  # TODO change new_key var to new_iv
                if len(new_key) % 16 == 0:
                    del msg[:16]  # "destroy" iv key

                    if new_key != old_iv_key:
                        try:
                            decrypted = aes.AES(KEY).decrypt_cbc(msg, new_key)
                            old_iv_key = new_key
                        except e:
                            print(f"check if IV is correct, or the problem is something else {e}")
                            # close connection or break loop because the data wouldn't be correct
                    # del msg[:16]  # "destroy" iv key

                    # dlt padding decode bek to string
                    # TODO close connection when gps data is successfully decode
                    decrypted = decrypted.decode().split("#", 1)
                    if decrypted[0] in ALL_NODE_ID.values():
                        node = get_key(decrypted[0])
                        if decrypted[1][0:6] == "$GPRMC":  # starting NMEA sentence, could add the V check of gps
                            nmea = pynmea2.parse(decrypted[1])
                            write_to_csv(nmea.data, node)

                        print(f"{addr} data= {decrypted}")
                        connected = False
        except UnicodeError:
            pass

    print(f"CLOSED CONN")
    # testing sleep not recommend may help with sim error but still weird
    time.sleep(2)
    logs = colored(sim,'red')
    print(logs)
    conn.close()


def start():
    server.listen(100)
    print(f"LISTING ON {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"active conn: {threading.active_count() - 1}")  # neglect start threading


print(f"server is start-ing up")
start()
